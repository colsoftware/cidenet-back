# CIDENET Employees Backend

Este es el backend de nuestra aplicación de administración y mantenimiento de empleados de CIDENET SAS.

Fué desarrollado con NodeJS y Bases de Datos Mongo

---
## Requerimientos

### Versión de Producción

Esta aplicación cuenta con una versión corriendo en Heroku como un adicional y para que sea visible en un entorno de producción. El enlace en el cuál se encuentra publicado este backend es: [Heroku link de producción](https://cinedet-heroku-backend.herokuapp.com/)

### Versión de Desarrollo

### Requerimientos

### Node: 

Instalar Node en su versión más estable. Acá tienes unas pequeñas instrucciones dependiendo de tu sistema operativo:

- #### Instalación en Windows

  Abre el [Sitio Web Oficial de Node](https://nodejs.org/) y descarga la versión estable del instalador. 

- #### Instalación en Ubuntu

  Para instalar Node en Ubuntu solo debes seguir y ejecutar los siguientes comandos:

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Otros Sistemas Operativos

  Para instalaciones en otros sistemas por favor sigue las instrucciones en las páginas oficiale de  [Node](https://nodejs.org/) y de [NPM](https://npmjs.org/).

Si la instalción se realizó correctamente puedes validar ejecutando el siguiente comando: (La versión puede varias dependiendo del equipo y el momento de la instalación).

    $ node --version
    v10.16.0

    $ npm --version
    6.9.2

###
### Mongo Compass
  Se recomienda que para trabajar con MongoDB se realice la instalación de Mongo Compass. No es obligatorio, puedes usar cualquier otro si deseas.

  El enlace con las instrucciones para cada sistema operativo las puedes encontrar en su [Página Oficial de Mongo Compass](https://www.mongodb.com/try/download/compass).

---

## Instalación del Proyecto

    $ git clone https://gitlab.com/colsoftware/cidenet-back.git
    $ cd cidenet-back
    $ npm install

    Una vez hayas ejecutado estos comandos procedemos a configurar la conexión de la Base de Datos con Mongo.

## Configuración del Proyecto

En la ruta de nuestro proyecto debemos abrir el archivo config.js que se encuentra en la ruta `server/config/config.js` y realizar los siguientes ajustes:

- En la variable process.env.PORT configuras el puerto con el que quieres trabajar. Por defecto el puerto de trabajo es el 3000;
- Debes ajustar la ruta local de Mongo con el puerto que tengas definido en tu base de datos. La ruta por defecto es mongodb://localhost:27017/employeesdb y el puerto por defecto es 27017;

## Correr el Proyecto
    Una ves instalado todos los requerimientos y realizadas las configuraciones podemos correr nuestro proyecto con el siguiente comando:

    $ npm start dev

    Si todo salió bien visualizarás un mensaje en consola indicando el puerto por el que estas trabajando y que la Base de Datos se encuentra ONLINE.

---

