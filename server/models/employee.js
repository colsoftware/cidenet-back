const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let validCountries = {
    values: ['Colombia', 'Estados Unidos'],
    message: '{VALUE} no es un país de empleo válido.'
}

let validDocumentTypes = {
    values: ['Cedula de Ciudadania', 'Cedula de Extranjeria', 'Pasaporte', 'Permiso Especial'],
    message: '{VALUE} no es un tipo de documento válido.'
}

let validAreas = {
    values: ['Administracion', 'Financiera', 'Compras', 'Infraestructura', 'Operacion', 'Talento Humano', 'Servicios Varios'],
    message: '{VALUE} no es un área válida.'
}

let employeeSchema = new Schema({
    lastName1: {
        type: String,
        required: [true, 'Primer apellido es un campo es requerido.'],
        maxlength: [20, 'Primer apellido solo puede tener máximo 20 caracteres.']
    },
    lastName2: {
        type: String,
        required: [true, 'Segundo apellido es un campo es requerido.'],
        maxlength: [20, 'Segundo apellido solo puede tener máximo 20 caracteres.']
    },
    name1: {
        type: String,
        required: [true, 'Primer nombre es un campo es requerido.'],
        maxlength: [20, 'Primer nombre solo puede tener máximo 20 caracteres.']
    },
    name2: {
        type: String,
        maxlength: [50, 'Otros nombres solo puede tener máximo 50 caracteres.']
    },
    country: {
        type: String,
        required: [true, 'Debe seleccionar un país de empleo.'],
        enum: validCountries
    },
    identificationType: {
        type: String,
        required: [true, 'Debe seleccionar un tipo de identificación.'],
        enum: validDocumentTypes
    },
    identificationNumber: {
        type: String,
        required: [true, 'Número de identificación es requerido.'],
        maxlength: [20, 'Número de identificación solo puede tener máximo 20 caracteres.']
    },
    email: {
        type: String,
        maxlength: [300, 'Correo electrónico solo puede tener máximo 300 caracteres.']
    },
    enterDate: {
        type: Date,
        required: [true, 'Fecha de ingreso es un campo requerido.']
    },
    area: {
        type: String,
        required: [true, 'Debe seleccionar un área.'],
        enum: validAreas
    },
    state: {
        type: Boolean,
        default: true
    },
    registerDate: {
        type: Date
    },
    editDate: {
        type: Date
    },
    createdby: {
        type: Schema.Types.ObjectId,
        ref: 'Users'
    }
});

module.exports = mongoose.model('Employees', employeeSchema);