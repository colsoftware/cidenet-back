const express = require('express');
const app = express();

app.use('/api/users', require('./users.routes'));
app.use('/api/employees', require('./employees.routes'));

module.exports = app;