/**
 *  Route: /api/employees
 **/

const { Router } = require('express');
const router = Router();

// Middlewares
const { verifyToken, verifyUserRole } = require('../middlewares/auth');

// Controllers
const { getAllEmployees, getEmployee, createEmployee, updateEmployee, deleteEmployee } = require('../controllers/employees.controller');

router.get('/', [verifyToken, verifyUserRole], getAllEmployees);
router.get('/:id', [verifyToken, verifyUserRole], getEmployee);
router.post('/create', [verifyToken, verifyUserRole], createEmployee);
router.put('/update/:id', [verifyToken, verifyUserRole], updateEmployee);
router.delete('/delete/:id', [verifyToken, verifyUserRole], deleteEmployee);


module.exports = router;