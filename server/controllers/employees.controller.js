const Employee = require('../models/employee');
var validator = require("email-validator");
var accents = require('remove-accents');

const getAllEmployees = (req, res) => {

    let state = req.query.state;

    Employee.find({ state: state }, 'name1 name2 lastName1 lastName2 country identificationType identificationNumber email enterDate area state')
        .sort('lastName1')
        .exec((err, employees) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                })
            }

            Employee.countDocuments({ state: state }, (err, count) => {
                if (err) {
                    return res.status(400).json({
                        ok: false,
                        err
                    })
                }

                res.json({
                    ok: true,
                    count: employees.length,
                    total: count,
                    data: employees
                });
            });
        });
};

const getEmployee = (req, res) => {
    let id = req.params.id;

    Employee.findById(id)
        .exec((err, employee) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                })
            }

            res.json({
                ok: true,
                data: employee
            });
        });
};

const createEmployee = (req, res) => {
    let body = req.body;

    // LowerCase
    body.name1 = body.name1.toLowerCase();
    body.name2 = body.name2.toLowerCase();
    body.lastName1 = body.lastName1.toLowerCase();
    body.lastName2 = body.lastName2.toLowerCase();

    // Remove accents
    body.name1 = accents.remove(body.name1);
    body.name2 = accents.remove(body.name2);
    body.lastName1 = accents.remove(body.lastName1);
    body.lastName2 = accents.remove(body.lastName2);

    // Replace ñ letter
    body.name1 = body.name1.replace('ñ', 'n');
    body.name2 = body.name2.replace('ñ', 'n');
    body.lastName1 = body.lastName1.replace('ñ', 'n');
    body.lastName2 = body.lastName2.replace('ñ', 'n');

    let employee = new Employee({
        name1: body.name1.toUpperCase(),
        name2: body.name2.toUpperCase(),
        lastName1: body.lastName1.toUpperCase(),
        lastName2: body.lastName2.toUpperCase(),
        country: body.country,
        identificationType: body.identificationType,
        identificationNumber: body.identificationNumber,
        email: '',
        enterDate: body.enterDate,
        area: body.area,
        registerDate: new Date().setHours(new Date().getHours() - 5), // Hour in Colombia
        editDate: new Date().setHours(new Date().getHours() - 5), // Hour in Colombia
        createdby: body.user
    });

    // Validate if employee exist
    Employee.find({ identificationType: employee.identificationType, identificationNumber: employee.identificationNumber, state: true })
        .exec((err, empCount) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                })
            }

            if (empCount && empCount.length > 0) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'Ya existe un empleado con el tipo y número de documento enviado.'
                    }
                });
            }

            employee.save((err, employeeDB) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    })
                }

                if (!employeeDB) {
                    return res.status(400).json({
                        ok: false,
                        err: {
                            message: 'Ha ocurrido un error al crear el empleado.'
                        }
                    })
                }

                // Generate employee email
                // Remove white spaces of name1 and lastName1

                let cleanName = body.name1.replace(/\s/g, "");
                let cleanLastName = body.lastName1.replace(/\s/g, "");
                let emailDomain = (body.country === 'Colombia') ? 'cidenet.com.co' : 'cidenet.com.us';

                let employeeEmail = `${ cleanName }.${ cleanLastName }@${ emailDomain }`;

                // Verify if email is unique
                Employee.countDocuments({ email: employeeEmail }, (err, count) => {
                    if (count > 0) {
                        // Add Unique Id to Email
                        let randomId = Math.ceil(Math.random() * 100);
                        employeeEmail = `${ cleanName }.${ cleanLastName }.${ randomId }@${ emailDomain }`;
                    }

                    // Validate and Update email in employee
                    if (validator.validate(employeeEmail) === true) {
                        employeeDB.email = employeeEmail;

                        employeeDB.save((err, emp) => {
                            if (err) {
                                return res.status(500).json({
                                    ok: false,
                                    err
                                })
                            }

                            if (!emp) {
                                return res.status(400).json({
                                    ok: false,
                                    err: {
                                        message: 'No ha sido posible asignar un email al empleado.'
                                    }
                                })
                            }

                            res.json({
                                ok: true,
                                data: emp
                            });
                        });
                    }
                });
            });
        });
};

const updateEmployee = (req, res) => {
    let id = req.params.id;
    let body = req.body;

    // LowerCase
    body.name1 = body.name1.toLowerCase();
    body.name2 = body.name2.toLowerCase();
    body.lastName1 = body.lastName1.toLowerCase();
    body.lastName2 = body.lastName2.toLowerCase();

    // Remove accents
    body.name1 = accents.remove(body.name1);
    body.name2 = accents.remove(body.name2);
    body.lastName1 = accents.remove(body.lastName1);
    body.lastName2 = accents.remove(body.lastName2);

    // Replace ñ letter
    body.name1 = body.name1.replace('ñ', 'n');
    body.name2 = body.name2.replace('ñ', 'n');
    body.lastName1 = body.lastName1.replace('ñ', 'n');
    body.lastName2 = body.lastName2.replace('ñ', 'n');

    Employee.findById(id)
        .exec((err, employeeDB) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                })
            }

            if (!employeeDB) {
                return res.status(404).json({
                    ok: false,
                    err: {
                        message: 'No se encontró información de ningún empleado.'
                    }
                })
            }

            if (employeeDB.name1 !== body.name1.toUpperCase() || employeeDB.lastName1 !== body.lastName1.toUpperCase() || employeeDB.country !== body.country) {
                // Generate employee email for new Names
                // Remove white spaces of name1 and lastName1

                let cleanName = body.name1.replace(/\s/g, "");
                let cleanLastName = body.lastName1.replace(/\s/g, "");
                let emailDomain = (body.country === 'Colombia') ? 'cidenet.com.co' : 'cidenet.com.us';

                let employeeEmail = `${ cleanName }.${ cleanLastName }@${ emailDomain }`;

                // Verify if email is unique
                Employee.countDocuments({ email: employeeEmail }, (err, count) => {
                    if (count > 0) {
                        // Add Unique Id to Email
                        let randomId = Math.ceil(Math.random() * 100);
                        employeeEmail = `${ cleanName }.${ cleanLastName }.${ randomId }@${ emailDomain }`;
                    }

                    // Validate and Update email in employee
                    if (validator.validate(employeeEmail) === true) {
                        employeeDB.email = employeeEmail;

                        // Update employee
                        employeeDB.name1 = body.name1.toUpperCase();
                        employeeDB.name2 = body.name2.toUpperCase();
                        employeeDB.lastName1 = body.lastName1.toUpperCase();
                        employeeDB.lastName2 = body.lastName2.toUpperCase();
                        employeeDB.country = body.country;
                        employeeDB.area = body.area;
                        employeeDB.editDate = new Date().setHours(new Date().getHours() - 5); // Hour in Colombia

                        employeeDB.save((err, emp) => {
                            if (err) {
                                return res.status(500).json({
                                    ok: false,
                                    err
                                })
                            }

                            if (!emp) {
                                return res.status(400).json({
                                    ok: false,
                                    err: {
                                        message: 'No se ha podido modificar el empleado.'
                                    }
                                })
                            }

                            res.json({
                                ok: true,
                                data: emp
                            });
                        });
                    }
                });
            } else {
                // Update employee
                employeeDB.name1 = body.name1.toUpperCase();
                employeeDB.name2 = body.name2.toUpperCase();
                employeeDB.lastName1 = body.lastName1.toUpperCase();
                employeeDB.lastName2 = body.lastName2.toUpperCase();
                employeeDB.country = body.country;
                employeeDB.area = body.area;
                employeeDB.editDate = new Date().setHours(new Date().getHours() - 5); // Hour in Colombia

                employeeDB.save((err, emp) => {
                    if (err) {
                        return res.status(500).json({
                            ok: false,
                            err
                        })
                    }

                    if (!emp) {
                        return res.status(400).json({
                            ok: false,
                            err: {
                                message: 'No se ha podido modificar el empleado.'
                            }
                        })
                    }

                    res.json({
                        ok: true,
                        data: emp
                    });
                });
            }
        });
};

const deleteEmployee = (req, res) => {
    let id = req.params.id;

    Employee.findByIdAndUpdate(id, { state: false }, { new: true, context: 'query' }, (err, deletedEmployee) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            })
        }

        if (!deletedEmployee) {
            return res.status(404).json({
                ok: false,
                err: {
                    message: 'El empleado no pudo ser borrado.'
                }
            })
        }

        res.json({
            ok: true,
            data: deletedEmployee
        });
    });
};

module.exports = { getAllEmployees, getEmployee, createEmployee, updateEmployee, deleteEmployee };